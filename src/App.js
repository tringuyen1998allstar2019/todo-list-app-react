import './App.css';
// import Baitap1 from './../src/baitap1/index';
// import Baitap2 from './../src/baitap2/index';
// import RenderingElements from './rendering-elements/index';
// import HandlingEvent from './handling-event/index';
// import ExampleHandlingEvent from './handling-event/example';
// import State from './state/index';
// import ExampleCar from './exampleState-car/index';
// import ListKeys from './list-keys/index';
// import ExampleList from './list-keys/example';
// import PraticeLoop from './praticeLoopHome/praticeLoop';
// import Props from './props/index';
// import ShoppingCart from './shopping-cart/index';
// import Usermanagement from './usermanagement/index';
// import HomeRedux from "./usermanagement-redux/index";
import HomeTodolist from "./todolist-homework/index";
// import FormValidation from "./form-validation/index";
// import LifeCycle from "./lifecycle/index";

function App() {
  return (
    <div className="App" >
      {/* <Baitap1 /> */}
      {/* <Baitap2 /> */}
      {/* <RenderingElements/> */}
      {/* <HandlingEvent/> */}
      {/* <ExampleHandlingEvent/> */}
      {/* <State/> */}
      {/* <ExampleCar/> */}
      {/* <ListKeys/> */}
      {/* <ExampleList/> */}
      {/* <PraticeLoop/> */}
      {/* <Props/> */}
      {/* <ShoppingCart/> */}
      {/* <Usermanagement/> */}
      {/* <HomeRedux/> */}
      <HomeTodolist/>
      {/* <FormValidation/> */}
      {/* <LifeCycle/> */}
 </div>
  );
}

export default App;
