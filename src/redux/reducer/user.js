import {DELETE_USER,EDIT_USER,GET_KEYWORD,SUBMIT_USER} from "./../constant";
let initialState = {
  data: [
    {
      id: 1,
      name: "Dinh Phuc Nguyen",
      username: "dpnguyen",
      email: "dpnguyen@gmail.com",
      phoneNumber: "1123123213",
      type: "VIP",
    },
    {
      id: 2,
      name: "Thương",
      username: "thươnglnm",
      email: "thuong9k@gmail.com",
      phoneNumber: "0366064643",
      type: "VIP",
    },
    {
      id: 3,
      name: "Trí",
      username: "trinm",
      email: "trinm@gmail.com",
      phoneNumber: "0943102053",
      type: "VIP",
    },
    {
      id: 4,
      name: "Tính",
      username: "ssssss",
      email: "nguyendp@gmail.com",
      phoneNumber: "56465465",
      type: "USER",
    },
    {
      id: 5,
      name: "Mmmcvm",
      username: "nguyendp",
      email: "ffff@gmail.com",
      phoneNumber: "90498465",
      type: "VIP",
    },
  ],
  userEdit: null,
  keyWord: "",
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SUBMIT_USER: {
      let newListUser = [...state.data];
      if(action.payload.id) {
        //update userEdit
        const index = newListUser.findIndex(user => user.id === action.payload.id);
        if(index !== -1) {
          newListUser[index] = action.payload;
        }
      } else {
        const userNew = {...action.payload,id:Math.random()};
        newListUser = [...state.data, userNew];
      }
      state.data = newListUser;
      return { ...state };
    }
    case GET_KEYWORD:{
      state.keyWord = action.payload;
      // let newListUser = [...state.data];
      // newListUser = newListUser.filter(user => user.name.toLowerCase().indexOf(keyWord))
      return { ...state };
    }

    case EDIT_USER: {
      state.userEdit = action.payload;
      return { ...state };
    }
    case DELETE_USER: {
      let newListUser = [...state.data];
      let index = state.data.findIndex((user) => user.id === action.payload.id);
      if (index !== -1) {
        newListUser.splice(index, 1);
        state.data = newListUser;
      }
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default userReducer;
