let initialState = {
  data: [
    {
      id: 1,
      task: "Lau Nhà",
      status: "completed",
    },
    {
      id: 2,
      task: "Đánh Răng",
      status: "completed",
    },
    {
      id: 3,
      task: "Cho mèo ăn",
      status: "uncompleted",
    },
    {
      id: 4,
      task: "Dọn chuồng",
      status: "uncompleted",
    },
    {
      id: 5,
      task: "Thể dục",
      status: "uncompleted",
    },
  ],
};

const taskReducer = (state = initialState, action) => {
  switch (action.type) {
    case "DELETE_TASK": {
      let newListTask = [...state.data];
      let index = newListTask.findIndex(
        (task) => task.id === action.payload.id
      );
      if (index !== -1) {
        newListTask.splice(index, 1);
        state.data = newListTask;
      }
      return { ...state };
    }
    
    case "ADD_TASK": {
      let newListTask = [...state.data];
      let newTask = {
        id: Math.random(),
        task: action.payload.task,
        status: "uncompleted"
      }
      newListTask.push(newTask);
      state.data = newListTask;
      return { ...state };
    }
    case "EDIT_STATUS": {
      let newListTask = [...state.data];
      let index = newListTask.findIndex(
        (task) => task.id === action.payload.id
      );
      let status = action.payload.status;
      if (status === "uncompleted") {
        newListTask[index].status = "completed";
      } else {
        newListTask[index].status = "uncompleted";
      }
      state.data = newListTask;
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default taskReducer;
