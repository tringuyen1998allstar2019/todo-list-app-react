import React, { Component } from "react";
import "./todolist.css";
import UncompletedTask from "./uncompletedTask";
import CompletedTask from "./completedTask";
import {connect} from "react-redux";
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
       task: ""
    }
  }
  handleOnChange = (event) =>   {
    const {name,value} = event.target
    this.setState({
      [name]: value
    }, ()=> {
      console.log(this.state);
    })
  }
  render() {
    return (
      <div className="card">
        <div className="card__header">
          <img src="./img/X2oObC4.png" />
        </div>
        {/* <h2>hello!</h2> */}
        <div className="card__body">
          <div className="card__content">
            <div className="card__title">
              <h2>My Tasks</h2>
              <p>September 9,2020</p>
            </div>
            <div className="card__add">
              <input
                id="newTask"
                type="text"
                placeholder="Enter an activity..."
                name="task"
                onChange={this.handleOnChange}
              />
              <button id="addItem" onClick={()=> {
                this.props.addNewTask(this.state);
              }}>
                <i className="fa fa-plus" />
              </button>
            </div>
            <div
              id="notiInput"
              className="alert-danger"
              style={{ display: "none" }}
            />
            <div className="card__todo">
              {/* Uncompleted tasks */}
              <UncompletedTask />
              {/* Completed tasks */}
              <CompletedTask />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
    return {
      addNewTask: (task) => {
        const action = {
          type: "ADD_TASK",
          payload: task
        }
        dispatch(action);
      }
    }
}
export default connect(null, mapDispatchToProps)(Home);
