import React, { Component } from "react";
import { connect } from "react-redux";

class CompletedTask extends Component {
  renderListCompleteTasks() {
    let { userList } = this.props;
    let listCompleteTasks = userList.filter(
      (todo) => todo.status === "completed"
    );

    return listCompleteTasks.map((item, index) => {
      // console.log(item);
      return (
        <li className="list-group-item d-flex align-items-center" key={index}>
          {item.task}
          <span>
            <i
              className="fa fa-trash-alt mr-2 text-danger"
              onClick={() => {
                this.props.deleteUser(item);
              }}
            />
            <i className="fa fa-check-circle" 
             onClick={() => {
              this.props.editStatus(item);
            }}/>
          </span>
        </li>
      );
    });
  }
  render() {
    return (
      <>
        <ul className="todo" id="completed">
          {this.renderListCompleteTasks()}
        </ul>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userList: state.taskReducer.data,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    deleteUser: (task) => {
      const action = {
        type: "DELETE_TASK",
        payload: task,
      };
      dispatch(action);
    },
    editStatus: (task) => {
      const action = {
        type: "EDIT_STATUS",
        payload: task,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CompletedTask);
