import React, { Component } from "react";
import { connect } from "react-redux";

class UncompletedTask extends Component {
  renderUncompleted = () => {
    let { taskList } = this.props;
    return taskList
      .filter((task) => task.status === "uncompleted")
      .map((item, index) => {
        return (
          <li className="list-group-item d-flex align-items-center" key={index}>
            {item.task}
            <span>
              <i className="fa fa-trash-alt mr-2 text-danger" onClick={() => {
                this.props.deleteUser(item);
              }}/>
              <i className="fa fa-check-circle" onClick={() =>{
                this.props.editStatus(item);
              }}/>
            </span>
          </li>
        );
      });
  };
  render() {
    return (
      <>
        <ul className="todo" id="todo">
          {this.renderUncompleted()}
        </ul>
      </>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    deleteUser:(task) =>{
      const action = {
        type: "DELETE_TASK",
        payload: task
      }
      dispatch(action)
    },
    editStatus:(task) =>{
      const action = {
        type: "EDIT_STATUS",
        payload: task
      }
      dispatch(action)
    }

  }
}
const mapStateToProps = (state) => {
  return {
    taskList: state.taskReducer.data,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(UncompletedTask);
